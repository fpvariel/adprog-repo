package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class FrenchCheese implements Cheese {

    public String toString() {
        return "Shredded Frech Cheese";
    }
}