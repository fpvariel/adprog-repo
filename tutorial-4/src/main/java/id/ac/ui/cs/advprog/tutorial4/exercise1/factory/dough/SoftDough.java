package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SoftDough implements Dough {
    public String toString() {
        return "Soft Dough";
    }
}
